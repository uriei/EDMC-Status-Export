# EDMC-Status Export
#### Description
This is a plugin for the awesome app [ED Market Connector (EDMC)](https://github.com/Marginal/EDMarketConnector), which in essence, allows the player to export some data from the Journal files to separated Text files, useful for Streamer overlays or chatbots, or whatever you think it could be useful.

#### Installation
On the Plugins settings tab in EDMC press the "Open" button. This reveals the plugins folder where EDMC looks for plugins.
Once there, create a folder named `StatusExport`, and drop the `load.py` file you should have downloaded from this repository, `\EDMarketConnector\plugins\StatusExport`.

(The folder name can be different but better not to mess with those things :D )

#### Usage
All exported files will be stored in the folder the user specifies on the settings tab, in case of any problem, it will default to EDMC's "Output" folder, which by default should be around `/Documents/Elite Dangerous/` (I think).
In EDMC settings the plugin will have its own tab, and there you will find 10 entry boxes (5 for filenames and 5 for content) for all 5 files it can create, whatever you type in the content ones is what it will be inside it's corresponding file, and you will see the list of available variables you can use, these will be replaced by the data you want to export, if available.

Also, it provides the player with a *Silent Mode* option in the main window to hide sensitive data from the files, like Location, Ship Name, and Ship ID, this option is meant for Streamers that want to hide these options if they have suspictions of someone doing Stream Sniping.

#### Current variable list
    %cmdr% - CMDR Name
    %location1%, %location2%, %location3% - Location with different levels of detail
    %gamemode% - Open, PG, Solo (Only if EDMC was running when the game loaded)
    %shipclass%, %shipname%, %shipid% - Your current ship
    %time_played% - Total play time.
    %thd_system%, %thd_time%, %thd_ship% - Your last reported hyperdiction.

#### Example
A file with this format: `CMDR %cmdr% is flying a %shipclass% named %shipname% with ID %shipid% in "%location3%".`

Would create a file with: `CMDR Uriei is flying a Type-9 Heavy named La Mula with ID UHV-01 in Merope 5 c at 5.1234 / -70.9876".`

Here's a visual example using Streamlabs Chatbot:
![Example chatbot commands](https://i.imgur.com/GEuqZUb.png "Example chatbot commands")
