import Tkinter as tk
import myNotebook as nb
import sys, os, plug, re, string
from config import config

this = sys.modules[__name__]	# For holding module globals

def plugin_prefs(parent, cmdr, is_beta):
    this.OutputPath = tk.StringVar(value=config.get("SE_OutputPath"))

    this.FirstFileN = tk.StringVar(value=config.get("SE_FirstFileN"))	# Retrieve file names
    this.SecondFileN= tk.StringVar(value=config.get("SE_SecondFileN"))
    this.ThirdFileN = tk.StringVar(value=config.get("SE_ThirdFileN"))
    this.ForthFileN = tk.StringVar(value=config.get("SE_ForthFileN"))
    this.FifthFileN = tk.StringVar(value=config.get("SE_FifthFileN"))

    this.FirstFileF = tk.StringVar(value=config.get("SE_FirstFileF"))	# Retrieve file contents
    this.SecondFileF= tk.StringVar(value=config.get("SE_SecondFileF"))
    this.ThirdFileF = tk.StringVar(value=config.get("SE_ThirdFileF"))
    this.ForthFileF = tk.StringVar(value=config.get("SE_ForthFileF"))
    this.FifthFileF = tk.StringVar(value=config.get("SE_FifthFileF"))

    frame = nb.Frame(parent)
    frame.columnconfigure(2, weight=1)
    PADY = 5

    nb.Label(frame, text="Enter the text and format for the exported files.\n"
    "Files will be exported to a subfolder inside EDMC's output folder\n"
    "if the Output path isn't set or it's invalid.").grid(column=0, columnspan=3, row=0, padx=0, pady=(0,PADY+10), sticky=tk.EW)
    nb.Label(frame, text="Output Path: ").grid(column=0, row=3, padx=0, pady=(0,PADY+5), sticky=tk.E)
    nb.Entry(frame, textvariable=this.OutputPath).grid(column=1, columnspan=3, row=3, padx=0, pady=(0,PADY), sticky=tk.EW)

    nb.Label(frame, text="Filename").grid(column=0, row=4, padx=0, pady=(0,PADY), sticky=tk.NW)
    nb.Label(frame, text="Content").grid(column=2, row=4, padx=0, pady=(0,PADY), sticky=tk.NW)

    nb.Entry(frame, textvariable=this.FirstFileN).grid(column=0, row=5, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Label(frame, text=".txt   ").grid(column=1, row=5, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Entry(frame, textvariable=this.FirstFileF).grid(column=2, columnspan=2, row=5, padx=0, pady=(0,PADY), sticky=tk.EW)

    nb.Entry(frame, textvariable=this.SecondFileN).grid(column=0, row=6, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Label(frame, text=".txt   ").grid(column=1, row=6, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Entry(frame, textvariable=this.SecondFileF).grid(column=2, row=6, padx=0, pady=(0,PADY), sticky=tk.EW)

    nb.Entry(frame, textvariable=this.ThirdFileN).grid(column=0, row=7, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Label(frame, text=".txt   ").grid(column=1, row=7, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Entry(frame, textvariable=this.ThirdFileF).grid(column=2, row=7, padx=0, pady=(0,PADY), sticky=tk.EW)

    nb.Entry(frame, textvariable=this.ForthFileN).grid(column=0, row=8, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Label(frame, text=".txt   ").grid(column=1, row=8, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Entry(frame, textvariable=this.ForthFileF).grid(column=2, row=8, padx=0, pady=(0,PADY), sticky=tk.EW)

    nb.Entry(frame, textvariable=this.FifthFileN).grid(column=0, row=9, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Label(frame, text=".txt   ").grid(column=1, row=9, padx=0, pady=(0,PADY), sticky=tk.EW)
    nb.Entry(frame, textvariable=this.FifthFileF).grid(column=2, row=9, padx=0, pady=(0,PADY), sticky=tk.EW)

    nb.Label(frame, text="").grid(column=0, columnspan=2, row=10, padx=0, pady=(0,PADY), sticky=tk.NW)
    nb.Label(frame, text="Available wildcards:").grid(column=0, columnspan=3, row=11, padx=0, pady=(0,PADY), sticky=tk.NW)
    nb.Label(frame, justify=tk.LEFT, text=\
        "%cmdr% - CMDR Name\n"
        "%location1%, %location2%, %location3% - Location with different levels of detail\n"
        "%gamemode% - Open, PG, Solo\n"
        "%shipclass%, %shipname%, %shipid% - Your current ship\n"
        "%time_played% - Total play time.\n"
        "%thd_system%, %thd_time%, %thd_ship% - Your last reported hyperdiction.").grid(column=0, columnspan=3, row=12, padx=0, pady=(0,PADY), sticky=tk.EW)

    return frame

def prefs_changed(cmdr, is_beta):
    global SE_filenames

    if this.OutputPath.get() != config.get('SE_OutputPath'):
        try:
            for i in range(len(SE_filenames)):
                try:
                    os.remove(SE_filenames[i])
                except Exception as e:
                    print "SE_Couldn't delete: " + str(e)
            try:
                os.rmdir(config.get('SE_OutputPath'))
            except:
                print "SE_Couldn't delete default output dir: " + str(e)
        except Exception as e:
            print "SE_Couldn't delete: " + str(e)
        finally:
            if this.OutputPath.get() != None and this.OutputPath.get() != "":
                config.set('SE_OutputPath', this.OutputPath.get())
            else:
                config.set('SE_OutputPath', config.get('outdir') + "\\Status Export\\")

    if this.FirstFileN.get() != config.get('SE_FirstFileN'):
        config.set('SE_FirstFileN', this.FirstFileN.get())
        try:
            os.remove(SE_filenames[0])
        except:
            print "SE_Couldn't delete: " + str(SE_filenames[0])
    if this.SecondFileN.get() != config.get('SE_SecondFileN'):
        config.set('SE_SecondFileN', this.SecondFileN.get())
        try:
            os.remove(SE_filenames[1])
        except:
            print "SE_Couldn't delete: " + str(SE_filenames[1])
    if this.ThirdFileN.get() != config.get('SE_ThirdFileN'):
        config.set('SE_ThirdFileN', this.ThirdFileN.get())
        try:
            os.remove(SE_filenames[2])
        except:
            print "SE_Couldn't delete: " + str(SE_filenames[2])
    if this.ForthFileN.get() != config.get('SE_ForthFileN'):
        config.set('SE_ForthFileN', this.ForthFileN.get())
        try:
            os.remove(SE_filenames[3])
        except:
            print "SE_Couldn't delete: " + str(SE_filenames[3])
    if this.FifthFileN.get() != config.get('SE_FifthFileN'):
        config.set('SE_FifthFileN', this.FifthFileN.get())
        try:
            os.remove(SE_filenames[4])
        except:
            print "SE_Couldn't delete: " + str(SE_filenames[4])

    config.set('SE_FirstFileF', this.FirstFileF.get())
    config.set('SE_SecondFileF', this.SecondFileF.get())
    config.set('SE_ThirdFileF', this.ThirdFileF.get())
    config.set('SE_ForthFileF', this.ForthFileF.get())
    config.set('SE_FifthFileF', this.FifthFileF.get())

    create_files("*Waiting for new data...*")

def plugin_start():
    global SE_Data
    global SilentMode
    global SE_Debug

    if config.get('SE_OutputPath') == None:
        config.set('SE_OutputPath', config.get('outdir') + "\\Status Export\\")
    SE_Debug = [False,config.get('SE_OutputPath') + "\\SE_DebugEntry.txt",config.get('SE_OutputPath') + "\\SE_DebugState.txt",config.get('SE_OutputPath') + "\\SE_DebugData.txt",config.get('SE_OutputPath') + "\\SE_DebugStatus.txt"]
    try:
        os.remove(SE_Debug[1])
        os.remove(SE_Debug[2])
        os.remove(SE_Debug[3])
        os.remove(SE_Debug[4])
    except:
        pass

    SilentMode = tk.IntVar()
    SilentMode.set(False)

    SE_Data = {}    #Establishing default dictionary status
    SE_Data["cmdr"] = "Unknown"
    SE_Data["shipclass"] = "Unknown"
    SE_Data["shipid"] = "Unknown"
    SE_Data["shipname"] = "Unknown"
    SE_Data["gamemode"] = "Unknown"
    SE_Data["location_type"] = "Unknown"
    SE_Data["location_system"] = "Unknown"
    SE_Data["location_station"] = "Unknown"
    SE_Data["location_body"] = "Unknown"
    SE_Data["thd_system"] = "Unknown"
    SE_Data["thd_time"] = "Unknown"
    SE_Data["thd_ship"] = "Unknown"
    SE_Data["time_played"] = "Unknown"
    SE_Data["Latitude"] = "%.4f"%0.0
    SE_Data["Longitude"] = "%.4f"%0.0
    SE_Data["Altitude"] = 0
    SE_Data["Heading"] = 0

    create_files("*Waiting for data...*")

    print "Status Export loaded!"
    return "Status Export"

def plugin_stop():
    create_files("*EDMC Status Export plugin isn't running*")
    #print "Status Export is out!"

def plugin_app(parent):
    global SilentMode
    this.SE_Silent = tk.Checkbutton(parent, variable=SilentMode, anchor=tk.W, text="Exporter Silent Mode", command=write_files)
    return (this.SE_Silent)

def write_files():
    global SE_Data
    global SilentMode
    global SE_PreviousExport
    global SE_filenames
    SE_patterns = [config.get('SE_FirstFileF'),config.get('SE_SecondFileF'),config.get('SE_ThirdFileF'),config.get('SE_ForthFileF'),config.get('SE_FifthFileF')]

    SE_pattern_replace = {
        "%cmdr%":       SE_Data["cmdr"],
        "%location1%":  SE_Data["location_system"],
        "%location2%":  SE_Data["location_body"],
        "%gamemode%":   SE_Data["gamemode"],
        "%shipclass%":  SE_Data["shipclass"],
        "%shipname%":   SE_Data["shipname"],
        "%shipid%":     SE_Data["shipid"],
        "%time_played%":SE_Data["time_played"],
        "%thd_system%": SE_Data["thd_system"],
        "%thd_time%":   SE_Data["thd_time"],
        "%thd_ship%":   SE_Data["thd_ship"]
    } # Replacements for export SE_patterns
    if SE_Data["location_type"] == "Body":
        SE_pattern_replace["%location3%"] = SE_Data["location_body"] + " at " + str(SE_Data["Latitude"]) + " / " + str(SE_Data["Longitude"])
    elif SE_Data["location_station"] != None:
        SE_pattern_replace["%location3%"] = SE_Data["location_station"]
        SE_pattern_replace["%location2%"] = SE_Data["location_station"]
    else:
        SE_pattern_replace["%location3%"] = SE_Data["location_system"]
        SE_pattern_replace["%location2%"] = SE_Data["location_system"]

    if SilentMode.get():
        SE_pattern_replace["%location3%"] = "*UNAVAILABLE*"
        SE_pattern_replace["%location2%"] = "*UNAVAILABLE*"
        SE_pattern_replace["%location1%"] = "*UNAVAILABLE*"
        SE_pattern_replace["%shipname%"] = "*UNAVAILABLE*"
        SE_pattern_replace["%shipid%"] = "*UNAVAILABLE*"

    try:
        for i in range(len(SE_filenames)):
            if SE_patterns[i] != "":
                #print "SE_Pattern " + str(i) + ": " + str(SE_patterns[i])
                SE_ToExport = SE_patterns[i]
                for o in range(len(SE_pattern_replace)):
                    SE_ToExport = string.replace(SE_ToExport,SE_pattern_replace.keys()[o],SE_pattern_replace.values()[o])
                    #print "SE_Pattern Replace \"" + str(SE_pattern_replace.keys()[o]) + "\" to \"" + str(SE_pattern_replace.values()[o])
                    #print "SE_Result at iteration " +str(i)+"/"+str(o)+": " + SE_ToExport
                if SE_PreviousExport[i] != SE_ToExport:
                    with open(SE_filenames[i],"w") as f:
                        f.write(SE_ToExport)
                    SE_PreviousExport[i] = SE_ToExport
    except Exception as e:
        print "SE_Couldn't write export file " + str(i+1) + " with content: " + str(SE_ToExport) + " | " + str(e)

def cmdr_data(data, is_beta):
    try:
        if SE_Debug[0]:
            with open(SE_Debug[3],"a") as f:
                f.write(str(data)+"\n")
    except:
        pass

def dashboard_entry(cmdr, is_beta, entry):
    global SE_Data
    #print "SE_Dash: " + str(entry)
    if entry["Flags"] & plug.FlagsHasLatLong:
        try: #Flight data if it exists
            SE_Data["Latitude"] = "%.4f"%(entry["Latitude"])
            SE_Data["Longitude"] = "%.4f"%(entry["Longitude"])
            SE_Data["Altitude"] = int(entry["Altitude"])
            SE_Data["Heading"] = "%03.f"%(entry["Heading"])
            SE_Data["location_type"] = "Body"
        except:
            SE_Data["Latitude"] = 0.0
            SE_Data["Longitude"] = 0.0
            SE_Data["Altitude"] = 0
            SE_Data["Heading"] = 0
            SE_Data["location_type"] = "System"
    else:
        SE_Data["location_type"] = "System"
        SE_Data["Latitude"] = 0.0
        SE_Data["Longitude"] = 0.0
        SE_Data["Altitude"] = 0
        SE_Data["Heading"] = 0
    try:
        if SE_Debug[0]:
            with open(SE_Debug[4],"a") as f:
                f.write(str(entry)+"\n")
    except:
        pass

    write_files()

def journal_entry(cmdr, system, station, entry, state):
    global SE_Data
    #print "SE_Journal: " + str(entry)
    #print "State: " + str(state)

    #Commander Name
    try:
        SE_Data["cmdr"] = cmdr
    except:
        print "SE_No CMDR info"

    #Body, Station and/or System name
    try:
        SE_Data["location_system"] = system
    except:
        pass
    try:
        if entry["event"] == "ApproachBody":
            SE_Data["location_body"] = entry["Body"]
    except:
        pass
    try:
        SE_Data["location_station"] = station
    except:
        pass

    #Gamemode, only works if EDMC was already running while joining the game.
    try:
        if entry.get('GameMode') != None:
            SE_Data["gamemode"] = entry.get('GameMode')
    except:
        pass

    #Last hyperdiction data
    try:
        SE_Data["thd_system"] = state["Statistics"]["TG_ENCOUNTERS"]["TG_ENCOUNTER_TOTAL_LAST_SYSTEM"]
        SE_Data["thd_time"] = state["Statistics"]["TG_ENCOUNTERS"]["TG_ENCOUNTER_TOTAL_LAST_TIMESTAMP"]
        SE_Data["thd_ship"] = state["Statistics"]["TG_ENCOUNTERS"]["TG_ENCOUNTER_TOTAL_LAST_SHIP"]
    except:
        pass

    #Total time played
    try:
        played_time_total_seconds = int(state["Statistics"]["Exploration"]["Time_Played"])
        played_time_seconds = int(played_time_total_seconds % 60)

        played_time_totalminutes = int(played_time_total_seconds / 60)
        played_time_minutes = int(played_time_totalminutes % 60)

        played_time_totalhours = int(played_time_totalminutes / 60)
        played_time_hours = int(played_time_totalhours % 24)

        played_time_totaldays = int(played_time_totalhours / 24)
        played_time_days = int(played_time_totaldays % 7)

        played_time_weeks = int(played_time_totaldays / 7)
        SE_Data["time_played"] = str(played_time_weeks) + "W " + str(played_time_days) + "D " + str(played_time_hours) + "H " + str(played_time_minutes) + "M"
    except:
        pass

    #Ship class, name and ID
    try:
        SE_Data["shipclass"] = state["ShipType"]
        SE_Data["shipclass"] = entry["ShipType"]
    except:
        pass
    try:
        SE_Data["shipname"] = state["ShipName"]
        SE_Data["shipname"] = entry["ShipName"]
    except:
        pass
    try:
        SE_Data["shipid"] = state["ShipIdent"]
        SE_Data["shipid"] = entry["ShipIdent"]
    except:
        pass
    #Now to correctly name the ship class
    SE_ShipClass_Replace = {
        "adder":                    "Adder",
        "anaconda":                 "Anaconda",
        "asp":                      "ASP Explorer",
        "asp_scout":                "ASP Scout",
        "belugaliner":              "Beluga Liner",
        "typex_3":                  "Alliance Challenger",
        "typex":                    "Alliance Chieftain",
        "typex_2":                  "Alliance Crusader",
        "cobramkiii":               "Cobra MkIII",
        "cobramkiv":                "Cobra MkIV",
        "diamondbackxl":            "Diamondback Explorer",
        "diamondback":              "Diamondback Scout",
        "dolphin":                  "Dolphin",
        "eagle":                    "Eagle",
        "federation_dropship_mkii": "Federal Assault Ship",
        "federation_corvette":      "Federal Corvette",
        "federation_dropship":      "Federal Dropship",
        "federation_gunship":       "Federal Gunship",
        "ferdelance":               "Fer-de-Lance",
        "hauler":                   "Hauler",
        "empire_trader":            "Imperial Clipper",
        "empire_courier":           "Imperial Courier",
        "cutter":                   "Imperial Cutter",
        "empire_eagle":             "Imperial Eagle",
        "independant_trader":       "Keelback",
        "krait_mkii":               "Krait MkII",
        "orca":                     "Orca",
        "python":                   "Python",
        "sidewinder":               "Sidewinder",
        "type6":                    "Type-6 Transporter",
        "type7":                    "Type-7 Transporter",
        "type9":                    "Type-9 Heavy",
        "type9_military":           "Type-10 Defender",
        "viper":                    "Viper MkIII",
        "viper_mkiv":               "Viper MkIV",
        "vulture":                  "Vulture"
    }
    try:
        SE_Data["shipclass"] = SE_ShipClass_Replace[SE_Data["shipclass"]]
    except:
        print "SE_Ship Class isn't registered: " + str(SE_Data["shipclass"])

    try:
        if SE_Debug[0]:
            with open(SE_Debug[1],"a") as f:
                f.write(str(entry)+"\n")
            with open(SE_Debug[2],"a") as f:
                f.write(str(state)+"\n")
    except:
        pass

    write_files()

def create_files(Def_Content): #Creates the export files with empty data (not empty really but like if so)
    global SE_filenames
    global SE_PreviousExport
    global SE_Data

    if config.get('SE_FirstFileN') == "" or config.get('SE_FirstFileN') == None:
        config.set('SE_FirstFileN', "Export_1")
    if config.get('SE_SecondFileN')== "" or config.get('SE_SecondFileN')== None:
        config.set('SE_SecondFileN', "Export_2")
    if config.get('SE_ThirdFileN') == "" or config.get('SE_ThirdFileN') == None:
        config.set('SE_ThirdFileN', "Export_3")
    if config.get('SE_ForthFileN') == "" or config.get('SE_ForthFileN') == None:
        config.set('SE_ForthFileN', "Export_4")
    if config.get('SE_FifthFileN') == "" or config.get('SE_FifthFileN') == None:
        config.set('SE_FifthFileN', "Export_5")
    SE_filenames = [config.get('SE_FirstFileN')+'.txt',config.get('SE_SecondFileN')+'.txt',config.get('SE_ThirdFileN')+'.txt',config.get('SE_ForthFileN')+'.txt',config.get('SE_FifthFileN')+'.txt']

    try:
        os.stat(config.get('SE_OutputPath'))
    except:
        try:
            os.makedirs(config.get('SE_OutputPath'))
            print "SE_Created export folder: " + config.get('SE_OutputPath')
        except Exception as e:
            print "SE_Couldn't create export folder, setting default output folder: " + str(e)
            try:
                config.set('SE_OutputPath', config.get('outdir') + "\\Status Export\\")
                os.makedirs(config.get('SE_OutputPath'))
            except Exception as e:
                print "SE_Couldn't create export folder on default output folder: " + str(e)

    try:
        for i in range(len(SE_filenames)):
            try:
                SE_filenames[i] = config.get('SE_OutputPath')+ "\\" + SE_filenames[i]
                with open(SE_filenames[i],"w") as f:
                    f.write(str(Def_Content))
            except Exception as e:
                print "SE_Couldn't create export files: " + str(e)
    except Exception as e:
        print "SE_Couldn't create export files: " + str(e)
    finally:
        SE_PreviousExport = []
        for i in range(len(SE_filenames)):
            SE_PreviousExport.append("")
